using System;
using System.Threading.Tasks;
using Application.Comments;
using MediatR;
using Microsoft.AspNetCore.SignalR;

namespace API.SignalR
{
    public class ChatHub : Hub
    {
        private readonly IMediator _mediator;
        public ChatHub(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task SendComment(Create.Command command)
        {
            var comment = await _mediator.Send(command).ConfigureAwait(false);

            await Clients.Group(command.ActivityId.ToString())
                    .SendAsync("ReceiveComment", comment.Value)
                    .ConfigureAwait(false);
        }

        public override async Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();
            var activityId = httpContext.Request.Query["activityId"];
            await Groups.AddToGroupAsync(Context.ConnectionId, activityId).ConfigureAwait(false);
            var result = await _mediator.Send(new List.Query { AcitvityId = Guid.Parse(activityId) });
            await Clients.Caller.SendAsync("LoadComments", result.Value).ConfigureAwait(false);
        }
    }
}